var express = require('express');
var app = express();
var mongojs = require('mongojs');
var db = mongojs('dictionary',['dictionary']);
var bodyParser = require('body-parser');

app.use(express.static(__dirname+'/public'));
app.use(bodyParser.json());

app.get('/dictionary', function(req,res){
	console.log("I received a GET request")

db.dictionary.count (function(err,dbcount){
	console.log(dbcount);
});

db.dictionary.find (function(err,docs){
	console.log(docs);
	res.json(docs);
});
});

app.post('/dictionary', function(req,res){
	console.log(req.body);
	db.dictionary.insert(req.body, function(err,doc){
		res.json(doc);
	});
});

app.delete('/dictionary/:id', function(req,res){
	var id = req.params.id;
	console.log(id);
	db.dictionary.remove({_id: mongojs.ObjectId(id)}, function(err, doc){
		res.json(doc);
	});
});

app.get('/dictionary/:id', function(req,res){
	var id = req.params.id;
	console.log(id);
	db.dictionary.findOne({_id: mongojs.ObjectId(id)}, function(err,doc){
		res.json(doc);
	});
});

app.put('/dictionary/:id', function(req,res){
	var id = req.params.id;
	console.log(req.body.wordOne);
	db.dictionary.findAndModify({query: {_id: mongojs.ObjectId(id)},
		update: {$set: {wordOne: req.body.wordOne, wordTwo: req.body.wordTwo}},	
		new: true}, function(err,doc){
			res.json();
	});
});

app.listen(5050);
console.log('server running 5050');