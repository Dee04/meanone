var app = angular.module("App1",['ui.bootstrap', 'ui.router', 'ngAnimate']);

app.config(['$stateProvider', '$urlRouterProvider', function($stateProvider, $urlRouterProvider){
	$stateProvider
	.state("login", {		
			url: '/login',					
			templateUrl: 'page-login.html',			
			controller: 'loginCtrl'
	}) 
	.state("register", {
		url: '/register',
		templateUrl: 'register.html',
		controller: 'registerCtrl'
	})
	.state("home", {
		url: '/home',
		templateUrl: 'home.html',
		controller: 'AppCtrl'
	})
		.state('otherwise', {
		url: "*path"
	})
}]);

app.controller('registerCtrl', function(){
	$('.srefLogin').hide();
});

app.controller('loginCtrl', ['$scope', '$state', '$timeout',
	function($scope, $state, $timeout){
		$('.srefLogin').hide();
		$scope.buttonLogin = function(){

			var x = $('#inputLogin').val();
			var y = $('#inputPassword').val();

			if(x=='a' && y=='a'){
				 $timeout(function() {
					$state.go('home');					
					}, 3000);
					swal({
					  title: "Auto start alert!",
					  text: "I will start in 3 seconds.",
					  timer: 2500,
					  showConfirmButton: false
					});			
			} else{
				swal("Upss...");
				};
			};		
}]);

app.controller("AppCtrl", function($scope, $http, $log){
	console.log("Hi!");

$('.srefLogin').hide();
var refresh = function(){

$http.get('/dictionary').success(function(response){
	console.log("test ok");
	console.log(response.length);
	$scope.countDatas = response.length;
	$scope.dictionary = response;
	$scope.lexicon = "";
});
};
refresh();

$scope.addTranslate = function(){
	console.log("ok");
	$http.post('/dictionary', $scope.lexicon).success(function(response){
		console.log(response);
		refresh();
	});
			swal({
  			title: "Great!",
			text: "You've been added a new translation :D",
			imageUrl: "images/add-ok.jpg"
});
};

// $scope.remove = function(id){
// 	console.log(id);
// 	$http.delete('/dictionary/' + id).success(function(response){
// 		$('.updateButton').hide();	
// 		$('.addButton').show();	
// 		refresh();
// 	});
// };
$scope.remove = function(id){
	console.log(id);
	swal({
	  title: "Are you sure?",
	  text: "You will not be able to recover this translation!",
	  type: "warning",
	  showCancelButton: true,
	  confirmButtonColor: "#DD6B55",
	  confirmButtonText: "Yes, delete it!",
	  closeOnConfirm: false
	},
	function(){
	  $http.delete('/dictionary/' + id).success(function(response){
	  $('.updateButton').hide();	
	  $('.addButton').show();	  
	  }); 
	  swal("Deleted!", "Your translation has been deleted.", "success");
	  refresh();
	});
			$('.updateButton').hide();	
			$('.addButton').show();	
			$('.clearButton').show();

};

$scope.edit = function(id){
	console.log(id);
	$('.updateButton').show();
	$http.get('/dictionary/' + id).success(function(response){
		$scope.lexicon = response;		
	});
};

$scope.update = function(){
	console.log("yeah!:D");
	$http.put('/dictionary/' + $scope.lexicon._id, $scope.lexicon).success(function(response){
		refresh();
	});
			$('.updateButton').hide();	
			$('.addButton').show();	
			$('.clearButton').show();
};

$scope.deselect = function(){
	$scope.lexicon = '';	
};

$scope.clickBox = function(position, dictionary) {
  angular.forEach(dictionary, function(lexicon, index) {
    if ((position != index) & ($('.checkBoxes').is(':checked'))){
      lexicon.checked = false;
      $('.updateButton').hide();	
		$('.addButton').hide();	
		$('.clearButton').hide();
		} else{
			$('.updateButton').hide();	
			$('.addButton').show();	
			$('.clearButton').show();
			$scope.lexicon = '';
    };
  });
};

$scope.hoverIn = function(id){
       $('.checkBoxes').show();

    };

$scope.hoverOut = function(id){
       $('.checkBoxes').hide();       
    };

$scope.addButton = function(){
	$('[data-toggle="tooltipAdd"]').tooltip(); 
};

$scope.clearButton = function(){
	$('[data-toggle="tooltipClear"]').tooltip(); 
};

$scope.updateButton = function(){
	$('[data-toggle="tooltipUpdate"]').tooltip(); 
};

$scope.editButton = function(){
	$('[data-toggle="tooltipEdit"]').tooltip(); 
};

$scope.removeButton = function(){
	$('[data-toggle="tooltipRemove"]').tooltip(); 
};

$scope.currentPage = 1;
$scope.numPerPage = 5;

$scope.paginate = function(value) {
	var begin, end, indexx;
    begin = ($scope.currentPage - 1) * $scope.numPerPage;
    end = begin + $scope.numPerPage;
    indexx = $scope.dictionary.indexOf(value);
    return (begin <= indexx && indexx < end);
    refresh();
  };
});

